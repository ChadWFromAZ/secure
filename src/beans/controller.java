package beans;

import java.security.Principal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@SessionScoped 
@ManagedBean(name = "Controller")
public class controller {
	
	public String onFlash (User user) {		
		FacesContext ctx = FacesContext.getCurrentInstance();
		String returnValue = ctx.getExternalContext().getRequestContextPath();
		ctx.getExternalContext().getFlash().put("user", (user.firstName + " " + user.lastName));		
		return "registered.xhtml?faces-redirect=true";
	}
	
	public String onLogoff() {
		// Invalidate the Session to clear the security token
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
		// Redirect to a protected page (so we get a full HTTP Request) to get Login Page
		return "TestResponse.xhtml?faces-redirect=true";

	}


	
}
