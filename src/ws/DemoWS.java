package ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class DemoWS {

	@WebMethod(operationName="whatWasCalled")
	public String returnText() {
		return "returnText() WS called";
	}
	
}
